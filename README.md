# UserScripts

UserScripts to enhance usability and add new functionality for multiple websites. Currently there are UserScripts for:

- [GitLab](https://gitlab.com/)
- [GitHub](https://github.com/)
- [Asana](https://asana.com/)



## Installation

1. Check what browser you are using. Each browser has an other way of installing UserScripts
   - In `Mozilla Firefox` / `Mozilla Waterfox` / `Mozilla Nightly` you can use (for comparison see below under `Greasemonkey vs Tampermonkey`)
     - for desktop
       - the Addon [Greasemonkey (Firefox Addon Store)](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/)
       - the Addon [Tampermonkey (Firefox Addon Store)](https://addons.mozilla.org/en-US/firefox/addon/tampermonkey/)  
         alternative [Tampermonkey (original site: Firefox)](https://www.tampermonkey.net/index.php?ext=dhdg&browser=firefox)
     - for mobile
       - the Addon [Greasemonkey (Firefox Addon Store)](https://addons.mozilla.org/en-US/android/addon/greasemonkey/)
       - the Addon [Tampermonkey (Firefox Addon Store)](https://addons.mozilla.org/en-US/android/addon/tampermonkey/)
   - In `Google Chrome` you can use the Addon [Tampermonkey (Chrome Addon Store)](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo)  
     alternative [Tampermonkey (original site: Google Chrome)](https://www.tampermonkey.net/index.php?ext=dhdg&browser=chrome)
   - In `Microsoft Edge` you can use the Addon [Tampermonkey (original site: Microsoft Edge)](https://www.tampermonkey.net/index.php?ext=dhdg&browser=edge)
   - In `Safari` you can use the Addon [Tampermonkey (original site: Safari)](https://www.tampermonkey.net/index.php?ext=dhdg&browser=safari)
   - In `Opera Next` you can use the Addon [Tampermonkey (original site: Opera Next)](https://www.tampermonkey.net/index.php?ext=dhdg&browser=opera)
1. Find a UserScript you want to install further down in this README under `UserScript Descriptions`
1. There you find the installation link/path
1. With this path, install the UserScript in your selected browser Addon
1. Try it & have fun :)



## Greasemonkey vs Tampermonkey

A short comparison:
- Greasemonkey was there first, long before Google Chrome even was born. It is developt until the current day in a closed manner by few people. It
  is only for Mozilla Firefox (desktop and mobile) and no other browsers. Script synchronization between multiple devices on multiple Firefox
  (desktop and mobile) Instances is buggy and only in new Greasemonkey versions avaiable.
- Tampermonkey is much newer. It is open source and everyone can participate. You can use it in Google Chrome, Microsoft Edge, Safari, Opera Next
  and Mozilla Firefox (desktop and mobile). Scripts can be synchronized with different methods (browser intern sync, Google Drive sync, ...). Some
  of these methods can not only sync between devices but also sync between different browsers (e.g. Firefox to Chrome, Opera to Firefox, ...).  
  (Under the hood old Tampermonkey versions use Greasemonkey to compile scripts.)

So what sould you use?
In the end it is up to you what you want to use. I personally recommend Tampermonkey because the last versions are much more stabel then
Greasemonkey and there are a lot more features. One of these features is script syncronization between devices and browsers.  
So I highly recommend enabling `Google Drive` syncronization in Tampermonkey in order to use the scripts in all browsers on all platforms.



## Repository construction

Short introduction of the folder structure:

- Folder `userscript`: Here are the UserScripts itself.
- Folder `resources`: Here are resources for the UserScripts and the repository itself like images for the wiki, icons, JavaScript libraries, ...
- Folder `.idea`: This is the workspace folder for jetbrain's IntelliJ IDEA for further developing.



## UserScript Descriptions

### for Asana

#### Asana - show existence of description in task overview
- name: `Asana - show existence of description in task overview`
- installation link/path: `https://gitlab.com/christopher2007/userscripts/raw/master/userscripts/build/Asana_show-existence-of-description-in-task-overview.min.js`
- description:  
  Shows an Icon on tasks in the task overview that indicates that the task has a description text.
- example
  - before  
    ![](docs/example-images/Asana_show-existence-of-description-in-task-overview_before.png)
  - after  
    ![](docs/example-images/Asana_show-existence-of-description-in-task-overview_after.png)
    There are three Icons possible in the right column:  
    - the rotating icon means "still working" -> script is still fetching data
    - text file icon -> there is a description in that task
    - no icon at all -> no description in that task present

### for GitLab

#### GitLab - absolute time instead of relative time
- name: `GitLab - absolute time instead of relative time`
- installation link/path: `https://gitlab.com/christopher2007/userscripts/raw/master/userscripts/build/GitLab_absolute-time-instead-of-relative-time.min.js`
- description:  
  Shows a better relative time then GitLab's time next to the absolute time in brackets after it in every context (comments, merge requests,
  commits, ...). The "better relative time" is because GitLab sees e.g. 2 months and 25 days as 2 months, not 3 months. This script rounds up on
  .5 on all time units.  
  Always uses your current timezone, does **not** show if there is a difference in the timezone to the other person that e.g. wrote the comment.  
  When the time is in the color red, that means it was modified/created less then 1 week ago, orange means less then 1 month ago. Older will be
  in the normal font color.
- example
  - before  
    ![](docs/example-images/GitLab_absolute-time-instead-of-relative-time_before.png)
  - after  
    ![](docs/example-images/GitLab_absolute-time-instead-of-relative-time_after.png)

#### GitLab - extended names for mentions in text
- name: `GitLab - extended names for mentions in text`
- installation link/path: `https://gitlab.com/christopher2007/userscripts/raw/master/userscripts/build/GitLab_extended-names-for-mentions-in-text.min.js`
- description:  
  Sets automatically the name of a issue after it is mentioned with `#<number>` Tag in a issue text, a wiki page, a merge-request text and in
  comments. Extends also all `!<number>` mentionings of merge-requests with the merge-request name.  
  If a Merge-Request ist already merged or a issue is already closed, then the mentioning gets a line through in order to cross it out.
- example
  - before  
    ![](docs/example-images/GitLab_extended-names-for-mentions-in-text_before.png)
  - after  
    ![](docs/example-images/GitLab_extended-names-for-mentions-in-text_after.png)

#### GitLab - new issue button in board view
- name: `GitLab - new issue button in board view`
- installation link/path: `https://gitlab.com/christopher2007/userscripts/raw/master/userscripts/build/GitLab_new-issue-button-in-board-view.min.js`
- description:  
  Adds a `new issue` button at the top of the ticket board.
- example
  - before  
    ![](docs/example-images/GitLab_new-issue-button-in-board-view_before.png)
  - after  
    ![](docs/example-images/GitLab_new-issue-button-in-board-view_after.png)

#### GitLab - remove welcome column in new boards
- name: `GitLab - remove welcome column in new boards`
- installation link/path: `https://gitlab.com/christopher2007/userscripts/raw/master/userscripts/build/GitLab_remove-welcome-column-in-new-boards.min.js`
- description:  
  Removes the 'welcome' column in new issue boards where you can add the 'doing' and 'to do' labels as columns with only one click.
- example
  - before  
    ![](docs/example-images/GitLab_remove-welcome-column-in-new-boards_before.png)
  - after  
    ![](docs/example-images/GitLab_remove-welcome-column-in-new-boards_after.png)
  
#### GitLab - add epic workflow and cross mentioning
- name: `GitLab - add epic workflow and cross mentioning`
- installation link/path: `https://gitlab.com/christopher2007/userscripts/raw/master/userscripts/build/GitLab_add-epic-workflow-and-cross-mentioning.min.js`
- description:  
  Adds workflow for epics and their sub-tasks (grouping):  
  You need a label `epic` and multiple labels `epic::Something`, `epic::dadada`, `epic::asd`, ... (this is called [scoped label set](https://gitlab.com/help/user/project/labels.md#scoped-labels-premium)
  and will work on payed GitLab, but this UserScript helps implementing the workflow also for non-payed repositories).  
  Now create a Meta-Issue that gets the `epic` and one of the other labels, e.g. `epic::Something`. This issue is the main Issue for the
  subsection `Something`. One Section always need exactly one main/meta issue like that and unlimited sub-issues.  
  Such a sub-issue only needs one of the `epic::` labels, here `epic::Something`.  
  Now the script does:
  - in the main epic issue the description gets extended right at the beginning with a list of all sub-issues and hyper links to them
  - in the sub-issues the description gets extended right at the beginning with a reference hyperlink to the main/meta epic issue
  - ~~the sub-issues names will be extended with the name of the main epic issues name in brackets as praefix~~
    (too many places for issue names, maybe in the future)
- example 1: the main epic issue
  - before  
    ![](docs/example-images/GitLab_add-epic-workflow-and-cross-mentioning_main-issue_before.png)
  - after  
    ![](docs/example-images/GitLab_add-epic-workflow-and-cross-mentioning_main-issue_after.png)
- example 2: a sub issue of an epic group
  - before  
    ![](docs/example-images/GitLab_add-epic-workflow-and-cross-mentioning_sub-issue_before.png)
  - after  
    ![](docs/example-images/GitLab_add-epic-workflow-and-cross-mentioning_sub-issue_after.png)
- example 3: a issue that is neither a main epic issue nor an sub issue of an epic group (=nothing)
  - before  
    ![](docs/example-images/GitLab_add-epic-workflow-and-cross-mentioning_none_before.png)
  - after  
    ![](docs/example-images/GitLab_add-epic-workflow-and-cross-mentioning_none_after.png)

### for GitHub

#### GitHub - absolute time instead of relative time
- name: `GitHub - absolute time instead of relative time`
- installation link/path: `https://gitlab.com/christopher2007/userscripts/raw/master/userscripts/build/GitHub_absolute-time-instead-of-relative-time.min.js`
- description:  
  Shows a better relative time then GitHub's time next to the absolute time in brackets after it in every context (comments, merge requests,
  commits, ...). The "better relative time" is because GitHub sees e.g. 2 months and 25 days as 2 months, not 3 months. This script rounds up on
  .5 on all time units.  
  Always uses your current timezone, does **not** show if there is a difference in the timezone to the other person that e.g. wrote the comment.  
  When the time is in the color red, that means it was modified/created less then 1 week ago, orange means less then 1 month ago. Older will be
  in the normal font color.
- example
  - before  
    ![](docs/example-images/GitHub_absolute-time-instead-of-relative-time_before.png)
  - after  
    ![](docs/example-images/GitHub_absolute-time-instead-of-relative-time_after.png)



## Other UserScript Databases

// TODO  
https://www.tampermonkey.net/scripts.php



## Missing features? Found a bug?

Just let me know in an issue or write me directly:  
`Thorsten Seyschab <business@i-thor.com>`



## Support the development
   
Do you like this project? Support it by donating via PayPal: [https://paypal.me/ThorstenSeyschab](https://paypal.me/ThorstenSeyschab)  
Thanks :)



## License

See `LICENSE` file.  
(General license comparison: [https://choosealicense.com/licenses/](https://choosealicense.com/licenses/))
