/*!
 * Copyright (C) - All Rights Reserved
 * Copyright start in January 2020 at project start, without temporal border.
 * Created in the course of the following Project:
 *      development name:         UserScripts
 *      final project name:       -
 *      final project website:    -
 *      project manager / leader: Thorsten Seyschab <business@i-thor.com> <http://i-thor.com/>
 * File only for this project, re-using in other projects strictly prohibited.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * [UTF-8 compatible (supports ä, ö, ü, ß, ...)]
 */

/*! Wird gebraucht um beim minimieren diesen Block für Tapermonkey/Greasemonkey/... nicht zu verlieren
// ==UserScript==
//
// @name            GitLab - extended names for mentions in text
// @name:en         GitLab - extended names for mentions in text
// @name:de         GitLab - erweiterte Namen bei Erwähnungen im Text
// @description     Sets automatically the name of a issue after it is mentioned with `#<number>` Tag in a issue text, a wiki page, a merge-request text and in comments. Extends also all `!<number>` mentionings of merge-requests with the merge-request name. If a Merge-Request ist already merged or a issue is already closed, then the mentioning gets a line through in order to cross it out.
// @icon            https://gitlab.com/christopher2007/userscripts/raw/master/resources/images/favicon_gitlab.png
//
// @author          Thorsten Seyschab <business@i-thor.com> <https://i-thor.com/>
// @namespace       https://i-thor.com/fixurl/project/userscripts/
//
// @license         GNU GPLv3 - http://www.gnu.org/licenses/gpl-3.0.txt
// @copyright       Copyright (C) 2019-2020, by Thorsten Seyschab <business@i-thor.com> <https://i-thor.com/>
//
// @match           https://gitlab.com/*
//
// @require         https://code.jquery.com/jquery-3.4.1.min.js
//
// @version         0.7.0
// @updateURL       https://gitlab.com/christopher2007/userscripts/raw/master/userscripts/build/GitLab_extended-names-for-mentions-in-text.min.js
// @downloadURL     https://gitlab.com/christopher2007/userscripts/raw/master/userscripts/build/GitLab_extended-names-for-mentions-in-text.min.js
//
// @run-at          document-end
// @unwrap
//
// ==/UserScript==
*/

// Import
import {
    addOwnStyle,
    domainCheck,
    launchHttpRequestSniffer,
    urlRegex,
    urlRegex_presets,
} from '../resources/scripts/userScriptsLib/userScriptsLib-0.0.1';

// Fängt direkt an, auch wenn die Seite noch nicht ganz geladen ist. Daher wird das kritische später in einem `$(document).ready(function() {});` gewrappt.
(function() {

    // Alle URLs sammeln, für die
    // 1. dieses Skript verwendet werden soll und
    // 2. die Variablen beinhalten, die man später haben möchte
    var allUrlsToUse = [ // Regex Gruppen sind die Variablen, die man später haben möchte. Von links nach rechts, da Regex Gruppen zu benennen in JavaScript nicht geht.

        // GitLab: Issue Detail Page
        urlRegex_presets.gitlabIssueDetailPage(),

        // GitLab: Wiki
        urlRegex_presets.gitlabWiki(),

        // GitLab: Merge Request
        urlRegex_presets.gitlabMergeRequest(),

    ];

    // URLs vorbereiten
    var domainCheckObj = new domainCheck(allUrlsToUse);

    // Nur weiter machen, wenn die URI der Seite stimmt (regex Prüfung auf die URI, da oben `@match` verwendet wurde, was entgegen `@include`
    // selbst kein direktes Regex unterstützt)
    if (!domainCheckObj.isDomainAccepted())
        return; // abbrechen

    // URL regex Gruppen für die IDE aufbereiten (nicht nötig, aber schöner zum programmieren)
    // -> [attention] if you used the `urlRegex_presets`, the variables here must also be fetched
    let username = domainCheckObj.getVariable("username");
    let repository = domainCheckObj.getVariable("repository");
    let stroke = domainCheckObj.getVariable("stroke");
    let issueId = domainCheckObj.getVariable("issueId");
    let wikiPageName = domainCheckObj.getVariable("wikiPageName");
    let mergeRequestId = domainCheckObj.getVariable("mergeRequestId");

    // Ansonsten alles weitere erst, wenn die Seite so weit geladen ist (statt UserScript Header `@run-at document-end`)
    $(document).ready(function() {

        // Manuelles erstes Starten des Skriptes nach wenigen Millisekunden
        setTimeout(function(){
            main();

            // Nach jedem erkannten HTTP Request zum Server das Skript noch mal ausführen (auf HTTP Requests horchen=sniffen)
            launchHttpRequestSniffer(function(ajaxRequestObj, requestArguments){
                main();
            });
        }, 200);

    });

    /**
     * Hauptfunktion. Beinhaltet das, was das UserScript eigentlich machen soll.
     */
    function main(){
        // Alle Elemente durchgehen
        $("a[data-original]" ).each(function( index ) {

            // interrupt if this mentioning was already extended
            if($(this).attr('i-thor-mentioning-extended') !== undefined)
                return;

            // Do nothing if it is not this exact construction that the regex says
            // Hier zerlegen in (Beispiel mit Erklärung siehe template): `<symbol><number>[ (<info>)]`
            const regex = /^(#|!)(\d+)( \([^\)]+\))?$/g;
            let [, symbol, number, info] = regex.exec($(this).text()) || [];
            if(!$(this).text().match(regex)) // Wenn der Grundaufbau komplett nicht vorhanden ist, abbrechen
                return;

            // set attribute to parent so that we can check on that later in order to not change this element again
            // (this is before the extending of the text in order not to hit a second thread that will change the same element -> parallelism)
            $(this).attr('i-thor-mentioning-extended', true);

            // in order to line through the mentioning when it is a issue that is closed, or a merge request that was already merged, ...
            var lineThrough = '';
            if(info !== undefined)
                lineThrough = ' style="text-decoration: line-through;"';

            // `info` ist optional
            if(info === undefined)
                info = ""; // leer, da sonst später der String `undefined` erzeugt werden würde

            // construct new text=content
            var innerText = '';
            if(symbol === "#") // ein Issue
                innerText = $(this).attr('title');
            else if(symbol === "!") // ein Merge Request
                innerText = $(this).attr('data-mr-title');
            var newText = `<span><span ${lineThrough}>${symbol}${number} ${innerText}</span>${info}</span>`;

            // set new text
            // $(this).text(newText); // only text, no html can be set
            $(this).html(newText); // can set html

        });
    }

})();
