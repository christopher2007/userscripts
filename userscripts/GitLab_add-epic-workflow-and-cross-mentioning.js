/*!
 * Copyright (C) - All Rights Reserved
 * Copyright start in January 2020 at project start, without temporal border.
 * Created in the course of the following Project:
 *      development name:         UserScripts
 *      final project name:       -
 *      final project website:    -
 *      project manager / leader: Thorsten Seyschab <business@i-thor.com> <http://i-thor.com/>
 * File only for this project, re-using in other projects strictly prohibited.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * [UTF-8 compatible (supports ä, ö, ü, ß, ...)]
 */

/*! Wird gebraucht um beim minimieren diesen Block für Tapermonkey/Greasemonkey/... nicht zu verlieren
// ==UserScript==
//
// @name            GitLab - add epic workflow and cross mentioning
// @name:en         GitLab - add epic workflow and cross mentioning
// @name:de         GitLab - füge Workflow für Epics und Kreuznennungen hinzu
// @description     description too long, therefore outsourced to: https://gitlab.com/christopher2007/userscripts/blob/master/README.md
// @icon            https://gitlab.com/christopher2007/userscripts/raw/master/resources/images/favicon_gitlab.png
//
// @author          Thorsten Seyschab <business@i-thor.com> <https://i-thor.com/>
// @namespace       https://i-thor.com/fixurl/project/userscripts/
//
// @license         GNU GPLv3 - http://www.gnu.org/licenses/gpl-3.0.txt
// @copyright       Copyright (C) 2019-2020, by Thorsten Seyschab <business@i-thor.com> <https://i-thor.com/>
//
// @match           https://gitlab.com/*
//
// @require         https://code.jquery.com/jquery-3.4.1.min.js
// @require         https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/js/all.min.js
//
// @version         0.1.0
// @updateURL       https://gitlab.com/christopher2007/userscripts/raw/master/userscripts/build/GitLab_add-epic-workflow-and-cross-mentioning.min.js
// @downloadURL     https://gitlab.com/christopher2007/userscripts/raw/master/userscripts/build/GitLab_add-epic-workflow-and-cross-mentioning.min.js
//
// @run-at          document-end
// @unwrap
//
// ==/UserScript==
*/

// Import
require("regenerator-runtime/runtime"); // in order to use `await` in `async function(){}`
import {
    domainCheck,
    urlRegex,
    urlRegex_presets,
    launchHttpRequestSniffer,
    GitLabApiWithCache,
} from '../resources/scripts/userScriptsLib/userScriptsLib-0.0.1';

// Fängt direkt an, auch wenn die Seite noch nicht ganz geladen ist. Daher wird das kritische später in einem `$(document).ready(function(){});` gewrappt.
(function(){

    // Alle URLs sammeln, für die
    // 1. dieses Skript verwendet werden soll und
    // 2. die Variablen beinhalten, die man später haben möchte
    var allUrlsToUse = [ // Regex Gruppen sind die Variablen, die man später haben möchte. Von links nach rechts, da Regex Gruppen zu benennen in JavaScript nicht geht.

        // GitLab: Issue Detail Page
        urlRegex_presets.gitlabIssueDetailPage(),

        //TODO all the following

        // // GitLab: Issue Board
        // urlRegex_presets.gitlabIssueBoard(),
        //
        // // GitLab: Issue List
        // urlRegex_presets.gitlabIssueList(),
        //
        // // GitLab: Milestone
        // urlRegex_presets.gitlabMilestone(),

        //TODO auch im Beschreibungstext von Issues und MergeRequests, wenn jemand nur `#4` schreibt! -> sync with other Plugin:
        // `GitLab_extended-names-for-mentions-in-text.js`

    ];

    // URLs vorbereiten
    var domainCheckObj = new domainCheck(allUrlsToUse);

    // Nur weiter machen, wenn die URI der Seite stimmt (regex Prüfung auf die URI, da oben `@match` verwendet wurde, was entgegen `@include`
    // selbst kein direktes Regex unterstützt)
    if (!domainCheckObj.isDomainAccepted())
        return; // abbrechen

    // URL regex Gruppen für die IDE aufbereiten (nicht nötig, aber schöner zum programmieren)
    // -> [attention] if you used the `urlRegex_presets`, the variables here must also be fetched
    let username = domainCheckObj.getVariable("username");
    let repository = domainCheckObj.getVariable("repository");
    let stroke = domainCheckObj.getVariable("stroke");
    let issueId = domainCheckObj.getVariable("issueId");
    let boardId = domainCheckObj.getVariable("boardId");
    let milestoneId = domainCheckObj.getVariable("milestoneId");

    // get global GitLab project Id
    var projectId = $("body").attr("data-project-id");

    // internal database for caching and also GitLab API
    var gitLabApiWithCache = new GitLabApiWithCache(projectId);

    /**
     * if this page is a issue detail page, it is for a specific issue. This variable is for holding all concrete information to this one issue the
     * page is from.
     *
     * @type issueObj|undefined
     */
    var currentIssue = undefined;

    // trash variable for short-time usages
    var tmp = undefined;

    // Ansonsten alles weitere erst, wenn die Seite so weit geladen ist (statt UserScript Header `@run-at document-end`)
    $(document).ready(async function(){

        // start after a few seconds in order to give GitLab a little bit more time for initialisation
        setTimeout(async function(){
            // init all variables
            await init();

            // start visible replacement once manually at page load
            await changeVisibleSiteForUser();

            // Nach jedem erkannten HTTP Request zum Server das Skript noch mal ausführen (auf HTTP Requests horchen=sniffen)
            launchHttpRequestSniffer(function(ajaxRequestObj, requestArguments){
                changeVisibleSiteForUser();
            });
        }, 300);

    });

    /**
     * helper function.
     * Inserts text at the beginning of the content description of a issue detail page.
     * (Different approach if there is already a description in the issue or not.)
     */
    function insertIntoDescriptionContainerAtTop(content){
        if($('div.detail-page-description div.description').length) // description exists, we can append
            content.insertBefore('div.detail-page-description div.description > *:first-child');
        else // description does not exist, so we must create our own div description container
            content.insertBefore('div.detail-page-description .edited-text');
    }

    /**
     * only call once on page load
     */
    async function init(){
        // Test on what site we are currently on
        if(location.href.match(urlRegex_presets.gitlabIssueDetailPage().regex)){ // we are on a site: issue detail page
            // fill `currentIssue` object with all information of the current issue detail page
            currentIssue = await gitLabApiWithCache.getIssueObj(issueId);
        }
    }

    /**
     * Call once manually on page load and then as often as you like, especially on GitLab page changes.
     */
    async function changeVisibleSiteForUser(){
        // Test on what site we are currently on
        if(location.href.match(urlRegex_presets.gitlabIssueDetailPage().regex)) { // we are on a site: issue detail page
            // interrupt if first init is not ready yet
            if(currentIssue === undefined)
                return;

            // interrupt if the description text was already extended
            if($("div[i-thor-epic-workflow-description-extended=\"true\"]").length)
                return;

            // Is the current issue an meta epic issue or an sub-issue of an epic or nothing of if?
            if(currentIssue.isEpicMain){ // main epic issue
                // Get all sub-issues that belong to this epic
                var subIssueIds = await gitLabApiWithCache.getFromLabelAllIssueIds(currentIssue.epicLabel);

                // iterate over all cached issues and print all issues that belong to this main epic issue
                var printHtml = '';
                for (const issueId of subIssueIds){
                    // don't print the epic issue itself in the own description
                    if(issueId == currentIssue.issueId)
                        continue;

                    // create bullet point with link to the issue
                    var tmp = await gitLabApiWithCache.getLinkToIssue(issueId);
                    printHtml = `${printHtml}<li>${tmp}</li>`;
                }
                if(printHtml == '') // empty, no sub-issues found
                    printHtml = '&lt;none&gt;';
                var textToExtend = $( `
                    <div i-thor-epic-workflow-description-extended="true">
                        <h4><i class="fas fa-trophy"></i> This is an 'epic' with the following sub-issues:</h4>
                        <ul>
                            ${printHtml}
                        </ul>
                        <h4><i class="far fa-sticky-note"></i> description</h4>
                    </div>`);

                // does this issue has a text description? (if not, the description div container is missing and we can not append it)
                insertIntoDescriptionContainerAtTop(textToExtend);
            }else if(currentIssue.isEpicSub){ // sub-issue from an epic
                // Get the main epic issue for this sub-issue
                var mainEpicIssueId = await gitLabApiWithCache.getFromEpicLabelMainEpicIssueId(currentIssue.epicLabel);

                // create html text to insert
                var printHtml = await gitLabApiWithCache.getLinkToIssue(mainEpicIssueId);
                var textToExtend = $( `
                    <div i-thor-epic-workflow-description-extended="true">
                        <span>
                            <i class="fas fa-trophy" style="color: #828282;"></i> This is an sub-issue to this epic:
                            ${printHtml}
                        </span>
                        <h4><i class="far fa-sticky-note"></i> description</h4>
                    </div>`);

                // does this issue has a text description? (if not, the description div container is missing and we can not append it)
                insertIntoDescriptionContainerAtTop(textToExtend);
            }else{ // not main epic issue and also not sub-issue from an epic: nothing
                var textToExtend = $( `
                    <div i-thor-epic-workflow-description-extended="true">
                        <span style="color: red;">
                            <i class="fas fa-times"></i> This issue neither is an main epic issue nor an sub-issue of an epic.
                        </span>
                        <h4><i class="far fa-sticky-note"></i> description</h4>
                    </div>`);
                insertIntoDescriptionContainerAtTop(textToExtend);
            }
        }
    }

})();
