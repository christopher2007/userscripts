/*!
 * Copyright (C) - All Rights Reserved
 * Copyright start in January 2020 at project start, without temporal border.
 * Created in the course of the following Project:
 *      development name:         UserScripts
 *      final project name:       -
 *      final project website:    -
 *      project manager / leader: Thorsten Seyschab <business@i-thor.com> <http://i-thor.com/>
 * File only for this project, re-using in other projects strictly prohibited.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * [UTF-8 compatible (supports ä, ö, ü, ß, ...)]
 */

/*! Wird gebraucht um beim minimieren diesen Block für Tapermonkey/Greasemonkey/... nicht zu verlieren
// ==UserScript==
//
// @name            GitLab - new issue button in board view
// @name:en         GitLab - new issue button in board view
// @name:de         GitLab - Neues Ticket Knopf in Board Ansicht
// @description     Adds a `new issue` button at the top of the ticket board.
// @icon            https://gitlab.com/christopher2007/userscripts/raw/master/resources/images/favicon_gitlab.png
//
// @author          Thorsten Seyschab <business@i-thor.com> <https://i-thor.com/>
// @namespace       https://i-thor.com/fixurl/project/userscripts/
//
// @license         GNU GPLv3 - http://www.gnu.org/licenses/gpl-3.0.txt
// @copyright       Copyright (C) 2019-2020, by Thorsten Seyschab <business@i-thor.com> <https://i-thor.com/>
//
// @match           https://gitlab.com/*
//
// @require         https://code.jquery.com/jquery-3.4.1.min.js
//
// @version         0.3.3
// @updateURL       https://gitlab.com/christopher2007/userscripts/raw/master/userscripts/build/GitLab_new-issue-button-in-board-view.min.js
// @downloadURL     https://gitlab.com/christopher2007/userscripts/raw/master/userscripts/build/GitLab_new-issue-button-in-board-view.min.js
//
// @run-at          document-end
// @unwrap
//
// ==/UserScript==
*/

// Import
import {
    addOwnStyle,
    domainCheck,
    urlRegex,
    urlRegex_presets,
} from '../resources/scripts/userScriptsLib/userScriptsLib-0.0.1';

// Fängt direkt an, auch wenn die Seite noch nicht ganz geladen ist. Daher wird das kritische später in einem `$(document).ready(function() {});` gewrappt.
(function() {

    // Alle URLs sammeln, für die
    // 1. dieses Skript verwendet werden soll und
    // 2. die Variablen beinhalten, die man später haben möchte
    var allUrlsToUse = [ // Regex Gruppen sind die Variablen, die man später haben möchte. Von links nach rechts, da Regex Gruppen zu benennen in JavaScript nicht geht.

        // GitLab: Issue Board
        urlRegex_presets.gitlabIssueBoard(),

    ];

    // URLs vorbereiten
    var domainCheckObj = new domainCheck(allUrlsToUse);

    // Nur weiter machen, wenn die URI der Seite stimmt (regex Prüfung auf die URI, da oben `@match` verwendet wurde, was entgegen `@include`
    // selbst kein direktes Regex unterstützt)
    if (!domainCheckObj.isDomainAccepted())
        return; // abbrechen

    // URL regex Gruppen für die IDE aufbereiten (nicht nötig, aber schöner zum programmieren)
    // -> [attention] if you used the `urlRegex_presets`, the variables here must also be fetched
    let username = domainCheckObj.getVariable("username");
    let repository = domainCheckObj.getVariable("repository");
    let stroke = domainCheckObj.getVariable("stroke");
    let boardId = domainCheckObj.getVariable("boardId");

    // Ansonsten alles weitere erst, wenn die Seite so weit geladen ist (statt UserScript Header `@run-at document-end`)
    $(document).ready(function() {

        // Link zum Formular für das Erstellen eines neuen Issues
        var linkForNewIssue = "https://gitlab.com/"+username+"/"+repository+"/issues/new";

        // Neuen Button erstellen und an die richtige Stelle setzen
        // eigentlich statt `<a>` ein `<button>`, aber dann bräuchte man ein onclick auf eine JavaScript Funktion ... so ist es einfacher :P
        var newIssueButton = $( "" +
            "<div class='board-extra-actions'>" +
            "   <a type='button' data-placement='bottom' title='' class='btn btn-success' data-original-title=''" +
            "       style='color: #fff; margin-left: 0.5rem;' href='"+linkForNewIssue+"'>" + // `style` und `href` sind in originalen Buttons in GitLab natürlich nicht enthalten, in meiner `a` Variante schon
            "       new issue" +
            "   </a>" +
            "</div>" );
        newIssueButton.insertBefore(".issues-filters > div > div:last-child > *:last-child");

    });

})();
