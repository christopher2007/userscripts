/*!
 * Copyright (C) - All Rights Reserved
 * Copyright start in January 2020 at project start, without temporal border.
 * Created in the course of the following Project:
 *      development name:         UserScripts
 *      final project name:       -
 *      final project website:    -
 *      project manager / leader: Thorsten Seyschab <business@i-thor.com> <http://i-thor.com/>
 * File only for this project, re-using in other projects strictly prohibited.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * [UTF-8 compatible (supports ä, ö, ü, ß, ...)]
 */

/*! Wird gebraucht um beim minimieren diesen Block für Tapermonkey/Greasemonkey/... nicht zu verlieren
// ==UserScript==
//
// @name            GitHub - absolute time instead of relative time
// @name:de         GitHub - absolute Zeiten statt reltive Zeiten
// @description     description too long, therefore outsourced to: https://gitlab.com/christopher2007/userscripts/blob/master/README.md
// @icon            https://gitlab.com/christopher2007/userscripts/raw/master/resources/images/favicon_github.png
//
// @author          Thorsten Seyschab <business@i-thor.com> <https://i-thor.com/>
// @namespace       https://i-thor.com/fixurl/project/userscripts/
//
// @license         GNU GPLv3 - http://www.gnu.org/licenses/gpl-3.0.txt
// @copyright       Copyright (C) 2019-2020, by Thorsten Seyschab <business@i-thor.com> <https://i-thor.com/>
//
// @match           https://github.com/*
//
// @require         https://code.jquery.com/jquery-3.4.1.min.js
// @//require         https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.25.3/moment.min.js
// @require         https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.25.3/moment-with-locales.min.js
// @require         https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.25.3/locale/de.min.js
// @//require         https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.31/moment-timezone.min.js
// @require         https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.31/moment-timezone-with-data.min.js
//
// @version         0.5.1
// @updateURL       https://gitlab.com/christopher2007/userscripts/raw/master/userscripts/build/GitHub_absolute-time-instead-of-relative-time.min.js
// @downloadURL     https://gitlab.com/christopher2007/userscripts/raw/master/userscripts/build/GitHub_absolute-time-instead-of-relative-time.min.js
//
// @run-at          document-end
// @unwrap
//
// ==/UserScript==
*/

// Import
import {
    domainCheck,
    launchHttpRequestSniffer,
    urlRegex,
} from '../resources/scripts/userScriptsLib/userScriptsLib-0.0.1';

// Fängt direkt an, auch wenn die Seite noch nicht ganz geladen ist. Daher wird das kritische später in einem `$(document).ready(function() {});` gewrappt.
(function() {

    // Alle URLs sammeln, für die
    // 1. dieses Skript verwendet werden soll und
    // 2. die Variablen beinhalten, die man später haben möchte
    var allUrlsToUse = [ // Regex Gruppen sind die Variablen, die man später haben möchte. Von links nach rechts, da Regex Gruppen zu benennen in JavaScript nicht geht.

        // Regex Aufbau der URL, hier alles erlaubt:   https://github.com/<irgendwas>
        //                                      z.B:   https://github.com/christopher2007/userscripts/
        new urlRegex(/^https:\/\/github\.com(\/.*)?$/, ["irgendwas"]),

    ];

    // URLs vorbereiten
    var domainCheckObj = new domainCheck(allUrlsToUse);

    // Nur weiter machen, wenn die URI der Seite stimmt (regex Prüfung auf die URI, da oben `@match` verwendet wurde, was entgegen `@include`
    // selbst kein direktes Regex unterstützt)
    if (!domainCheckObj.isDomainAccepted())
        return; // abbrechen

    // URL regex Gruppen für die IDE aufbereiten (nicht nötig, aber schöner zum programmieren)
    // -> [attention] if you used the `urlRegex_presets`, the variables here must also be fetched
    let irgendwas = domainCheckObj.getVariable("irgendwas");

    // Ansonsten alles weitere erst, wenn die Seite so weit geladen ist (statt UserScript Header `@run-at document-end`)
    $(document).ready(function() {
        // Sprache für `moment.js` setzen
        if(navigator.language === 'de' || navigator.language === 'de-de')
            moment.updateLocale('de');

        // Manuelles erstes Starten des Skriptes nach wenigen Millisekunden
        setTimeout(function(){
            main();
        }, 100);

        // Nach jedem erkannten HTTP Request zum Server das Skript noch mal ausführen (auf HTTP Requests horchen=sniffen)
        launchHttpRequestSniffer(function(ajaxRequestObj, requestArguments){
            main();
        });

    });

    /**
     * Hauptfunktion. Beinhaltet das, was das UserScript eigentlich machen soll.
     */
    function main(){
        // Alle `time-ago` Elemente mit dem Attribut `datetime` durchgehen, diese sind auf jeden Fall für eine Zeitanzeige zuständig
        $("time-ago[datetime]").each(function( index ) {
            // Direkt die Zeit nutzen und den Inhalt des DOM Elementes neu setzen
            setContentWithTime($(this), $(this).attr('datetime'));
        });

        // Alle `relative-time` Elemente mit dem Attribut `datetime` durchgehen, diese sind auf jeden Fall für eine Zeitanzeige zuständig
        $("relative-time[datetime]").each(function( index ) {
            // Direkt die Zeit nutzen und den Inhalt des DOM Elementes neu setzen
            setContentWithTime($(this), $(this).attr('datetime'));
        });
    }

    /**
     * Hilfsfunktion, ersetzt konkret den Inhalt von DOM Elementen mit der neuen Zeit.
     *
     * @param elem Object Das jQuery Element, von welchem der Textinhalt ausgetauscht werden soll.
     * @param timeString String Die Ziel-Zeit als String.
     */
    function setContentWithTime(elem, timeString){
        // Zeitzone des aktuellen Nutzers
        // var timezone = moment.tz.guess();
        var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;

        // JavaScript DateTime in eine moment.js Instanz umwandeln
        var time = moment(timeString);

        // Aktuelle zeit
        var now = moment();

        // Zeit in die aktuelle Zeitzone überführen
        time.tz(timezone);
        now.tz(timezone);

        // Text neu setzen
        // $(elem).text(time.format('YYYY-MM-DD HH:mm:ss'));
        // $(elem).text(time.format('YYYY-MM-DD HH:mm'));
        $(elem).text(`~${time.fromNow(true)} (${time.format('YYYY-MM-DD HH:mm')})`);

        // Farbe des Textes setzen
        var diff = time.diff(now) * (-1);
        if(diff < 604800000) // Kleiner einer Woche
            $(elem).css('color', '#ff0000');
        else if(diff < (4*604800000)) // Kleiner einem Monat
            $(elem).css('color', '#ffa200');
    }

})();
