/*!
 * Copyright (C) - All Rights Reserved
 * Copyright start in January 2020 at project start, without temporal border.
 * Created in the course of the following Project:
 *      development name:         UserScripts
 *      final project name:       -
 *      final project website:    -
 *      project manager / leader: Thorsten Seyschab <business@i-thor.com> <http://i-thor.com/>
 * File only for this project, re-using in other projects strictly prohibited.
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * [UTF-8 compatible (supports ä, ö, ü, ß, ...)]
 */

/*! Wird gebraucht um beim minimieren diesen Block für Tapermonkey/Greasemonkey/... nicht zu verlieren
// ==UserScript==
//
// @name            Asana - show existence of description in task overview
// @name:en         Asana - show existence of description in task overview
// @name:de         Asana - zeige Existenz von Beschreibungen in Aufgabenübersicht
// @description     Shows an Icon on tasks in the task overview that indicates that the task has a description text.
// @icon            https://gitlab.com/christopher2007/userscripts/raw/master/resources/images/favicon_asana.png
//
// @author          Thorsten Seyschab <business@i-thor.com> <https://i-thor.com/>
// @namespace       https://i-thor.com/fixurl/project/userscripts/
//
// @license         GNU GPLv3 - http://www.gnu.org/licenses/gpl-3.0.txt
// @copyright       Copyright (C) 2019-2020, by Thorsten Seyschab <business@i-thor.com> <https://i-thor.com/>
//
// @match           https://app.asana.com/*
//
// @require         https://code.jquery.com/jquery-3.4.1.min.js
// @require         https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/js/all.min.js
//
// @version         0.6.1
// @updateURL       https://gitlab.com/christopher2007/userscripts/raw/master/userscripts/build/Asana_show-existence-of-description-in-task-overview.min.js
// @downloadURL     https://gitlab.com/christopher2007/userscripts/raw/master/userscripts/build/Asana_show-existence-of-description-in-task-overview.min.js
//
// @run-at          document-start
// @unwrap
//
// ==/UserScript==
*/

// Import
import {
    addOwnStyle,
    domainCheck,
    urlRegex,
} from '../resources/scripts/userScriptsLib/userScriptsLib-0.0.1';

// Fängt direkt an, auch wenn die Seite noch nicht ganz geladen ist. Daher wird das kritische später in einem `$(document).ready(function() {});` gewrappt.
(function() {

    // Wird mehrmals benötigt
    var urlRegexTaskList = /^https:\/\/app\.asana\.com\/([^\/]+)\/(\d+)\/list$/;

    // Alle URLs sammeln, für die
    // 1. dieses Skript verwendet werden soll und
    // 2. die Variablen beinhalten, die man später haben möchte
    var allUrlsToUse = [ // Regex Gruppen sind die Variablen, die man später haben möchte. Von links nach rechts, da Regex Gruppen zu benennen in JavaScript nicht geht.

        // Regex Aufbau der URL, hier "Task List":  https://app.asana.com/<kp>/<boardId>/list
        //                                    z.B:  https://app.asana.com/0/1146334911267339/list
        new urlRegex(urlRegexTaskList, ["kp", "boardId"]),

        // Regex Aufbau der URL, hier "Task Detail":  https://app.asana.com/<kp>/<boardId>/<taskId>
        //                                      z.B:  https://app.asana.com/0/1146334911267339/1157731227732609
        new urlRegex(/^https:\/\/app\.asana\.com\/([^\/]+)\/(\d+)\/(\d+)$/, ["kp", "boardId", "taskId"]),

    ];

    // URLs vorbereiten
    var domainCheckObj = new domainCheck(allUrlsToUse);

    // Nur weiter machen, wenn die URI der Seite stimmt (regex Prüfung auf die URI, da oben `@match` verwendet wurde, was entgegen `@include`
    // selbst kein direktes Regex unterstützt)
    if (!domainCheckObj.isDomainAccepted())
        return; // abbrechen

    // URL regex Gruppen für die IDE aufbereiten (nicht nötig, aber schöner zum programmieren)
    // -> [attention] if you used the `urlRegex_presets`, the variables here must also be fetched
    let kp = domainCheckObj.getVariable("kp");
    let boardId = domainCheckObj.getVariable("boardId");
    let taskId = domainCheckObj.getVariable("taskId");

    // Eigenen Style hinzufügen
    addOwnStyle(`<style type='text/css'>
            /* Loading Spinner */
            div[role=row]:not(.SpreadsheetGridHeaderStructure) > div:first-child > div:first-child > div:last-child:not(.i-thor-noLoadingImage){
                /*background-image: url("https://gitlab.com/christopher2007/userscripts/raw/master/resources/images/icon_spinner.gif");*/
                background-image: url("https://i.ibb.co/5jGf5kk/icon-spinner.gif");
                background-repeat: no-repeat;
                background-position: right center;
                background-size: 18px;
            }

            /* Text-Icon als Indikator, dass es für diesen Task einen Beschreibungstext gibt */
            .i-thor-iconWrapper {
                width: 36px;
                min-width: 36px;
                max-width: 36px;
                display: flex;
                flex-direction: row;
                justify-content: center;
                align-items: center;
                border-top: 1px solid #E8ECEE;
            }
            .i-thor-iconWrapper > i{
                font-size: 1.2em;
            }
            .removed{
                background-color: red !important;
            }
        </style>`);

    // Ansonsten alles weitere erst, wenn die Seite so weit geladen ist (statt UserScript Header `@run-at document-end`)
    $(document).ready(function() {

        // Eventlistener aktivieren, um URL Änderungen abfangen zu können
        // window.addEventListener('click', function(){ // Geht NICHT für Elemente, die dynamisch nachgeladen oder nacherstellt werden
        //     console.log("1");
        // });
        $(document.body).on('click', 'a.NavigationLink', function() { // Hört nur auf die Menü-Einträge links, die Seitennavigationen darstellen
            setTimeout(function(){ // Sonst kommt meine Prüfung vor der Domainänderung von Asana, was nicht sein darf
                recognizeNewTaskList(location.href);
            }, 800);
        });

        // Direkt für das aktuelle Board ausführen -> Mit Verzögerung, da asana mit Sockets arbeitet und asynchron nachlädt
        setTimeout(function(){
            recognizeNewTaskList(location.href);
        }, 2500);

    });

    var oldUrl = null; // die aktuelle URL cachen, um später damit vergleichen zu können
    var tasksInUrl = null;
    function recognizeNewTaskList(newUrl){
        // Nur dann die Elemente neu laden, wenn die URL auf ein neues Issue-Board hinweist
        let [,, boardIdOld,] = urlRegexTaskList.exec(oldUrl) || [];
        let [,, boardIdNew,] = urlRegexTaskList.exec(newUrl) || [];
        if(boardIdOld === boardIdNew)
            return; // Immer noch die Selbe Aufgabenliste
        oldUrl = newUrl; // neue URL als alte cachen
        tasksInUrl = {}; // bereits geladene Tasks zurücksetzen

        // Eventlistener aktivieren, um Scrollen vom Nutzer abfangen zu können
        $(".SpreadsheetGridScroller-verticalScroller").scroll(function(){
            loadNewTasks();
        });

        // Für dieses Board vor dem ersten Scrollen des Nutzers schon alles, was er sieht, bearbeiten
        loadNewTasks();
    }

    function loadNewTasks(){
        // Sicherheitshalber mit kurzer Verzöterung, falls die sockets von Asana zu langsam arbeiten
        setTimeout(function(){
            // Alle Elemente durchgehen
            $("textarea[id^=Pot]" ).each(function( index ) {
                // Aktuelles Objekt cachen
                var currentObj = $(this);

                // Die taskId (=gid) des tasks ermitteln
                const taskIdRegex = /^Pot\.(\d+)_(\d+)_(\d+)$/;
                let [, projectId, kp, taskId] = taskIdRegex.exec($(this).attr('id')) || [];

                // Wurde dieser Task auf der aktuellen Seite bereits ermittelt?
                if(taskId in tasksInUrl){
                    // wurde schon angefragt, daher kein weiterer Serveraufruf notwendig.
                    drawDescriptionIcon(currentObj, taskId);
                    return;
                }

                // TaskID jetzt schon als angefragt markieren, um dopplungen zu verhindern, falls der ajax Aufruf langsam ist
                tasksInUrl[taskId] = false; // wir tun erst mal so, als hätte es keine Beschreibung

                // GET Anfrage an den Server um Informationen zu diesem Task zu ermitteln
                $.ajax({
                    url: "https://app.asana.com/api/1.0/tasks/" + taskId,
                    // data: data,
                    // success: success,
                    // dataType: "application/json",
                }).done(function (data){
                    // Gibt es hier einen Beschreibungstext?
                    tasksInUrl[taskId] = (data.data.notes !== undefined && data.data.notes !== "");
                    drawDescriptionIcon(currentObj, taskId);
                });
            });
        }, 800);
    }

    function drawDescriptionIcon(currentObj, taskId){
        // Alle alten Icons löschen
        currentObj.closest("div[role=row]").find(".i-thor-iconWrapper").remove();

        // Icon darstellen wenn notwendig
        if(tasksInUrl[taskId]){
            // currentObj.closest("div[role=row] > div:first-child > div:first-child").addClass("hasDescription");
            var descriptionIcon = $( `
                                <div class='i-thor-iconWrapper'>
<!--                                    <i class="fa fa-file-text-o" aria-hidden="true"></i>-->
                                    <i class="far fa-file-alt"></i>
                                </div>` );
            descriptionIcon.insertAfter(currentObj.closest("div[role=row] > div:first-child > div:first-child"));
        }

        // Ladeanimation entfernen
        currentObj.closest("div[role=row] > div:first-child > div:first-child").children("div").last().addClass("i-thor-noLoadingImage");
    }

})();
